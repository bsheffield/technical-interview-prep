{
 "cells": [
  {
   "cell_type": "markdown",
   "source": [
    "##  Tree Height\n",
    "\n",
    "In this problem we consider binary trees, represented by pointer data structures.\n",
    "\n",
    "A binary tree is either an empty tree or a node (called the root) consisting of a single integer value and two further binary trees, called the left subtree and the right subtree.\n",
    "\n",
    "For example, the figure below shows a binary tree consisting of six nodes. Its root contains the value 5, and the roots of its left and right subtrees have the values 3 and 10, respectively. The right subtree of the node containing the value 10, as well as the left and right subtrees of the nodes containing the values 20, 21 and 1, are empty trees.\n",
    "\n",
    "![](pics/tree_height.png)\n",
    "\n",
    "A path in a binary tree is a non-empty sequence of nodes that one can traverse by following the pointers. The length of a path is the number of pointers it traverses. More formally, a path of length K is a sequence of nodes P[0], P[1], ..., P[K], such that node P[I + 1] is the root of the left or right subtree of P[I], for 0 ≤ I < K. For example, the sequence of nodes with values 5, 3, 21 is a path of length 2 in the tree from the above figure. The sequence of nodes with values 10, 1 is a path of length 1. The sequence of nodes with values 21, 3, 20 is not a valid path.\n",
    "\n",
    "The height of a binary tree is defined as the length of the longest possible path in the tree. In particular, a tree consisting of only one node has height 0 and, conventionally, an empty tree has height −1. For example, the tree shown in the above figure is of height 2.\n",
    "\n",
    "### Problem\n",
    "Write a function:\n",
    "\n",
    "```def solution(T)```\n",
    "\n",
    "that, given a non-empty binary tree T consisting of N nodes, returns its height. For example, given tree T shown in the figure above, the function should return 2, as explained above. Note that the values contained in the nodes are not relevant in this task.\n",
    "\n",
    "### Technical details\n",
    "A binary tree can be given using a pointer data structure. Assume that the following declarations are given:\n",
    "\n",
    "```class Tree(object):\n",
    "  x = 0\n",
    "  l = None\n",
    "  r = None\n",
    "```\n",
    "\n",
    "An empty tree is represented by an empty pointer (denoted by None). A non-empty tree is represented by a pointer to an object representing its root. The attribute x holds the integer contained in the root, whereas attributes l and r hold the left and right subtrees of the binary tree, respectively.\n",
    "\n",
    "For the purpose of entering your own test cases, you can denote a tree recursively in the following way. An empty binary tree is denoted by None. A non-empty tree is denoted as (X, L, R), where X is the value contained in the root and L and R denote the left and right subtrees, respectively. The tree from the above figure can be denoted as:\n",
    "\n",
    "  (5, (3, (20, None, None), (21, None, None)), (10, (1, None, None), None))\n",
    "Assumptions\n",
    "Write an efficient algorithm for the following assumptions:\n",
    "\n",
    "N is an integer within the range [1..1,000];\n",
    "the height of tree T (number of edges on the longest path from root to leaf) is within the range [0..500]."
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% md\n"
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "class Tree(object):\n",
    "    x = 0\n",
    "    l = None\n",
    "    r = None\n",
    "\n",
    "\n",
    "def solution(T):\n",
    "    if T:\n",
    "        return max(solution(T.l), solution(T.r)) + 1\n",
    "    return -1\n",
    "\n"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "## TreeLongestZigZag\n",
    "\n",
    "In this problem we consider binary trees. Let's define a turn on a path as a change in the direction of the path (i.e. a switch from right to left or vice versa). A zigzag is simply a sequence of turns (it can start with either right or left). The length of a zigzag is equal to the number of turns.\n",
    "\n",
    "Consider binary tree below:\n",
    "\n",
    "![](pics/tree_longest_zigzag.png)\n",
    "\n",
    "There are two turns on the marked path. The first one is at [15]; the second is at [30]. That means that the length of this zigzag is equal to 2. This is also the longest zigzag in the tree under consideration. In this problem you should find the longest zigzag that starts at the root of any given binary tree and form a downwards path.\n",
    "\n",
    "Note that a zigzag containing only one edge or one node has length 0.\n",
    "\n",
    "### Problem\n",
    "Write a function:\n",
    "\n",
    "```def solution(T)```\n",
    "\n",
    "that, given a non-empty binary tree T consisting of N nodes, returns the length of the longest zigzag starting at the root.\n",
    "\n",
    "For example, given tree T shown in the figure above, the function should return 2, as explained above. Note that the values contained in the nodes are not relevant in this task.\n",
    "\n",
    "### Technical details\n",
    "A binary tree can be specified using a pointer data structure. Assume that the following declarations are given:\n",
    "\n",
    "```class Tree(object):\n",
    "  x = 0\n",
    "  l = None\n",
    "  r = None\n",
    "```\n",
    "\n",
    "An empty tree is represented by an empty pointer (denoted by None). A non-empty tree is represented by a pointer to an object representing its root. The attribute x holds the integer contained in the root, whereas attributes l and r hold the left and right subtrees of the binary tree, respectively.\n",
    "\n",
    "For the purpose of entering your own test cases, you can denote a tree recursively in the following way. An empty binary tree is denoted by None. A non-empty tree is denoted as (X, L, R), where X is the value contained in the root and L and R denote the left and right subtrees, respectively. The tree from the above figure can be denoted as:\n",
    "\n",
    "  (5, (3, (20, (6, None, None), None), None), (10, (1, None, None), (15, (30, None, (9, None, None)), (8, None, None))))\n",
    "Assumptions\n",
    "Write an efficient algorithm for the following assumptions:\n",
    "\n",
    "N is an integer within the range [1..100,000];\n",
    "the height of tree T (number of edges on the longest path from root to leaf) is within the range [0..800]."
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% md\n"
    }
   }
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
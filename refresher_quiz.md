1. Arrays and Strings
True/False:
T/F: The time complexity of accessing an element in a dynamic array (like Python's list) is O(1).
T/F: The time complexity of finding the maximum element in an unsorted array is O(log n).
T/F: Strings in Python are immutable, meaning once created, they cannot be modified in place.
T/F: The sliding window technique is only applicable to fixed-size windows.
Multiple Choice:
What is the time complexity of appending an element to a Python list?

A) O(1) on average
B) O(n)
C) O(log n)
D) O(n^2)
Which of the following techniques is most suitable for finding the longest substring without repeating characters?

A) Two-pointer technique
B) Binary search
C) Depth-first search (DFS)
D) Breadth-first search (BFS)
Given a sorted array, which algorithm can you use to find an element in O(log n) time?

A) Linear search
B) Binary search
C) Depth-first search (DFS)
D) Breadth-first search (BFS)
2. Linked Lists
True/False:
T/F: In a singly linked list, you can traverse the list in both directions.
T/F: The time complexity to insert an element at the head of a singly linked list is O(1).
T/F: A doubly linked list requires less memory than a singly linked list because it stores two pointers per node.
Multiple Choice:
What is the time complexity of reversing a singly linked list?

A) O(1)
B) O(n)
C) O(n log n)
D) O(n^2)
Which of the following operations is faster in a singly linked list compared to an array?

A) Random access of elements
B) Insertion at the end
C) Insertion at the head
D) Deletion of the last element
How do you detect a cycle in a linked list?

A) Two-pointer technique (slow and fast pointers)
B) Binary search
C) Depth-first search (DFS)
D) Breadth-first search (BFS)
3. Stacks and Queues
True/False:
T/F: A stack follows the First-In-First-Out (FIFO) principle.
T/F: A queue can be implemented using two stacks.
T/F: The time complexity of pushing an element onto a stack is O(n).
Multiple Choice:
Which of the following is a valid application of a stack?

A) Breadth-first search (BFS)
B) Depth-first search (DFS)
C) Level-order traversal of a tree
D) Dijkstra’s algorithm
Which data structure is used to implement a Breadth-First Search (BFS) algorithm?

A) Stack
B) Queue
C) Heap
D) Tree
Which of the following operations is NOT typically associated with a stack?

A) Push
B) Pop
C) Peek
D) Enqueue
4. Hash Maps and Hash Sets
True/False:
T/F: Hash maps provide O(1) average time complexity for insertion, deletion, and search operations.
T/F: A hash collision occurs when two different keys map to the same hash value.
T/F: Python's dict is an example of a hash map.
Multiple Choice:
Which of the following is NOT a valid use case for a hash map?

A) Counting frequencies of elements in an array
B) Implementing a priority queue
C) Detecting duplicates in an array
D) Caching results of expensive computations
What is the time complexity of checking if an element exists in a hash set?

A) O(log n)
B) O(n)
C) O(1)
D) O(n log n)
Which of the following is a common technique to handle hash collisions?

A) Binary search
B) Chaining
C) Depth-first search (DFS)
D) Sorting
5. Trees
True/False:
T/F: The height of a balanced binary search tree (BST) is O(log n).
T/F: In a binary search tree, the left child of a node contains values greater than the node.
T/F: The time complexity of searching for an element in a binary search tree is O(log n) in the best case.
Multiple Choice:
Which tree traversal method visits nodes in the order: left subtree, root, right subtree?

A) Pre-order
B) In-order
C) Post-order
D) Level-order
What is the time complexity of inserting an element into a balanced binary search tree (BST)?

A) O(1)
B) O(log n)
C) O(n)
D) O(n log n)
Which of the following data structures is the most suitable for implementing a priority queue?

A) Binary search tree (BST)
B) Linked list
C) Heap
D) Hash set
6. Graphs
True/False:
T/F: A graph can be represented using an adjacency matrix or an adjacency list.
T/F: Depth-first search (DFS) can be used to detect cycles in a graph.
T/F: Dijkstra’s algorithm can handle graphs with negative edge weights.
Multiple Choice:
What is the time complexity of Breadth-First Search (BFS) on a graph with V vertices and E edges?

A) O(V)
B) O(E)
C) O(V + E)
D) O(V log E)
Which of the following algorithms is used to find the shortest path in a weighted graph?

A) Depth-first search (DFS)
B) Breadth-first search (BFS)
C) Dijkstra’s algorithm
D) Topological sort
Which algorithm is used to find the minimum spanning tree (MST) in a graph?

A) Kruskal’s algorithm
B) Depth-first search (DFS)
C) Breadth-first search (BFS)
D) Floyd-Warshall algorithm
7. Dynamic Programming
True/False:
T/F: Dynamic programming is only applicable to problems that exhibit the property of overlapping subproblems.
T/F: The time complexity of the Fibonacci sequence using dynamic programming with memoization is O(n).
T/F: A greedy algorithm is always optimal for problems that can be solved using dynamic programming.
Multiple Choice:
Which of the following problems can be solved using dynamic programming?

A) Longest Common Subsequence
B) Depth-first search (DFS)
C) Breadth-first search (BFS)
D) Binary search
Which of the following is a characteristic of a problem that can be solved using dynamic programming?

A) The problem can be broken down into independent subproblems.
B) The problem can be solved using a divide-and-conquer approach.
C) The problem has overlapping subproblems and optimal substructure.
D) The problem can be solved using a greedy approach.
What is the time complexity of solving the 0/1 Knapsack Problem using dynamic programming?

A) O(n)
B) O(n^2)
C) O(nW), where n is the number of items and W is the capacity of the knapsack
D) O(n log n)
8. Backtracking
True/False:
T/F: Backtracking is commonly used to solve optimization problems.
T/F: The N-Queens problem can be solved using backtracking.
T/F: Backtracking explores all possible solutions and discards those that do not satisfy the constraints.
Multiple Choice:
Which of the following problems can be solved using backtracking?

A) N-Queens Problem
B) Dijkstra’s Algorithm
C) Binary Search
D) Merge Sort
What is the time complexity of solving the N-Queens problem using backtracking?

A) O(n)
B) O(n!)
C) O(n^2)
D) O(log n)
Which of the following is a key characteristic of backtracking?

A) It only explores one solution path at a time and backtracks when a dead end is reached.
B) It always finds the optimal solution.
C) It uses dynamic programming to store previously computed results.
D) It guarantees a solution in polynomial time.
Answers:
1. Arrays and Strings

T/F: 1. True, 2. False, 3. True, 4. False
MCQ: 1. A, 2. A, 3. B
2. Linked Lists

T/F: 1. False, 2. True, 3. False
MCQ: 1. B, 2. C, 3. A
3. Stacks and Queues

T/F: 1. False, 2. True, 3. False
MCQ: 1. B, 2. B, 3. D
4. Hash Maps and Hash Sets

T/F: 1. True, 2. True, 3. True
MCQ: 1. B, 2. C, 3. B
5. Trees

T/F: 1. True, 2. False, 3. True
MCQ: 1. B, 2. B, 3. C
6. Graphs

T/F: 1. True, 2. True, 3. False
MCQ: 1. C, 2. C, 3. A
7. Dynamic Programming

T/F: 1. True, 2. True, 3. False
MCQ: 1. A, 2. C, 3. C
8. Backtracking

T/F: 1. False, 2. True, 3. True
MCQ: 1. A, 2. B, 3. A